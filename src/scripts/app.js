let jobsData = [];
let filteredSkills = [];

const headerElement = document.createElement('header');
const mainElement = document.createElement('main');

function renderHeader() {
  const headOneElement = document.createElement('h1');
  document.body.prepend(headerElement);

  headOneElement.textContent = 'Job Listings';
  headOneElement.ariaHidden = true;

  headerElement.append(headOneElement);
}

function renderMain() {
  headerElement.after(mainElement);
}

function contractRender(postedAt, contract, location) {
  const contractWrapper = document.createElement('ul');
  const postedElement = document.createElement('li');
  const contractElement = document.createElement('li');
  const locationElement = document.createElement('li');

  contractWrapper.classList.add('contract');

  postedElement.textContent = postedAt;
  contractElement.textContent = contract;
  locationElement.textContent = location;

  contractWrapper.append(postedElement);
  contractWrapper.append(contractElement);
  contractWrapper.append(locationElement);

  return contractWrapper;
}

function infoWrapper(company, newOffer = false, featured = false) {
  const infoWrapper = document.createElement('div');
  const companyElement = document.createElement('span');

  infoWrapper.classList.add('info');
  companyElement.classList.add('company');

  companyElement.textContent = company;

  infoWrapper.append(companyElement);

  if (newOffer) {
      const newElement = document.createElement('span');
      newElement.classList.add('new');
      newElement.textContent = 'New!';
      infoWrapper.append(newElement);
    }

  if (featured) {
    const featuredElement = document.createElement('span');
    featuredElement.classList.add('featured');
    featuredElement.textContent = 'Featured';
    infoWrapper.append(featuredElement);
  }

  return infoWrapper;
}

function skillsWrapper({ role, level, languages, tools }) {
  const skills = [role, level, ...languages, ...tools];
  const skillsElement = document.createElement('ul');

  skillsElement.classList.add('skills');

  skills.forEach(skill => {
    const skillElement = document.createElement('li');

    skillElement.classList.add('skill');
    skillElement.dataset.skill = skill;
    skillElement.textContent = skill;

    skillsElement.append(skillElement);
  })

  skillsElement.addEventListener('click', filterJobs);

  return skillsElement;
}

function filterSkills(skill) {
  const setSkills = jobsData.filter(({ role, level, languages, tools }) => {
    if (role.includes(skill) || level.includes(skill) || languages.includes(skill) || tools.includes(skill)) {
      return true;
    }
    return false;
  });

  jobsData = setSkills;
  return setSkills;
}

function filterJobs(event) {
  const { target: { dataset: { skill = '' } = {} } } = event;
  if (filteredSkills.includes(skill) || !skill) {
    return;
  }

  filteredSkills = [...filteredSkills, skill];

  filterSkills(skill);

  clearRenderJobs();
  filterInfo();
  createJobs(jobsData);
}

function filterInfo() {
  const filterWrapper = document.createElement('div');
  const skillsFiltered = document.createElement('ul')
  const clearFilterElement = document.createElement('button');
  skillsFiltered.classList.add('skills-filtered');
  clearFilterElement.type = 'button';
  clearFilterElement.name = 'clear';
  clearFilterElement.classList.add('clear-all');
  clearFilterElement.textContent = 'Clear';

  filteredSkills.map(skill => {
    const skillFiltered = document.createElement('li');
    const clearSkill = document.createElement('span');

    skillFiltered.classList.add('skill');
    skillFiltered.dataset.skill = skill;
    skillFiltered.textContent = skill;

    clearSkill.classList.add('clear');
    clearSkill.textContent = 'X';

    skillFiltered.append(clearSkill);
    skillsFiltered.append(skillFiltered);
  });

  filterWrapper.append(skillsFiltered);
  filterWrapper.append(clearFilterElement);
  filterWrapper.classList.add('filterWrapper');

  filterWrapper.addEventListener('click', async (event) => {
    const removeSkill = event.target.parentNode.dataset.skill;
    if (!removeSkill) return;

    filteredSkills = filteredSkills.filter(skill => skill !== removeSkill);

    event.target.parentNode.remove();
    await fetchJson();
    filteredSkills.forEach(skill => {
      filterSkills(skill);
    });

    if (!filteredSkills.length) {
      clearAll();
      return;
    }

    clearRenderJobs();
    filterInfo();
    createJobs(jobsData);
  });
  clearFilterElement.addEventListener('click', clearAll);

  mainElement.append(filterWrapper);
}

async function clearAll(event) {
  event?.stopPropagation();
  const filtrWrapper = document.querySelector('.filterWrapper');
  filteredSkills = [];
  await fetchJson();
  clearRenderJobs();
  createJobs(jobsData);
  filtrWrapper.remove();
}

function createJobs(jobs) {
  jobs.forEach((job) => {
    const { logo, company, new: newOffer = false,
    featured = false, position, postedAt, contract, location } = job;
    const wrapperDiv = document.createElement('div');
    const imgElement = document.createElement('img');
    const detailsElement = document.createElement('div');
    const positionElement = document.createElement('h2');

    // classes
    wrapperDiv.classList.add('wrapper-job');
    detailsElement.classList.add('details');
    positionElement.classList.add('position');

    if (postedAt.includes('1d')) {
      wrapperDiv.classList.add('today');
    }

    // content
    imgElement.src = logo;
    imgElement.alt = company;
    positionElement.textContent = position;

    // inject
    wrapperDiv.append(imgElement);
    wrapperDiv.append(detailsElement);
    wrapperDiv.append(skillsWrapper(job));

    detailsElement.append(infoWrapper(company, newOffer, featured));
    detailsElement.append(positionElement);
    detailsElement.append(contractRender(postedAt, contract, location));

    mainElement.append(wrapperDiv);
  });
}

function clearRenderJobs() {
  const wrapperJob = document.querySelectorAll('.wrapper-job');
  const filterBlock = document.querySelector('.filterWrapper');
  if (filterBlock) {
    filterBlock.remove();
  }
  wrapperJob.forEach(job => job.remove());
}

async function fetchJson() {
  const response = await fetch("./src/../data.json");
  const data = await response.json();
  jobsData = data;
}

async function initJobs() {
  await fetchJson();
  renderMain();
  createJobs(jobsData);
}

function init() {
  renderHeader();
}

init();
initJobs();



