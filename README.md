# Frontend Mentor - Job listings with filtering solution

This is a solution to the [Job listings with filtering challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/job-listings-with-filtering-ivstIPCt). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [Time spent](#time-spent)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Filter job listings based on the categories

### Screenshot

[<img src="./src/images/job.png" width="500"/>](./src/images/job.png)

### Links

- Solution URL: [Job Listings](https://gitlab.com/balmor/job-listings)
- Live Site URL: [Job Listings](https://balmor.gitlab.io/job-listings/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

### Time spent
- 1 day (1h)
- 2 day (4h)
- 3 day (2.5h)
- 4 day (1.5h)
- 5 day (1.5h)

Total hours: 10,5h

## Author

- Website - [Damian Duda](https://balmor.github.io/)
- Frontend Mentor - [@balmor](https://www.frontendmentor.io/profile/balmor)
